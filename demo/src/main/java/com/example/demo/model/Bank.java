package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "bank")
public class Bank {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    //@Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

//    @Column(name = "phone")
//    private String surname;
//
//    @Column(name = "email")
//    private String email;
//
//    @Column(name = "address")
//    private String address;
//
//    @Column(name = "shop_id")
//    private Long shop_id;
//
//    @Column(name = "voen")
//    private String voen;

}
