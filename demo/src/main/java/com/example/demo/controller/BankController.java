package com.example.demo.controller;

import com.example.demo.model.Bank;
import com.example.demo.service.BankServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/bank")

public class BankController {
    private final BankServiceImpl bankService;

    @GetMapping
    public List<Bank> getAllEmployees() {
        return bankService.getAllEmployee();
                //employeeService.getAllEmployee();
    }

    @GetMapping("/{id}")
    public Bank getEmployeeById(@PathVariable("id") Long id) {
        log.info("id {}",id);
        return bankService.getEmployeeById(id);
        //employeeService.getEmployeeById(id);
    }

//    @PostMapping()
//    public ResponseEntity<Void> createEmployee(@RequestBody @Valid EmployeeDto employee ) throws MethodArgumentNotValidException {
//        employeeService.createEmployee(employee);
//        return ResponseEntity.status(HttpStatus.CREATED).build();
//    }
//
//    @PutMapping("/{id}")
//    public void updateEmployee(@PathVariable Long id, @RequestBody EmployeeDto employeeDto) {
//        employeeService.updateEmployee(id, employeeDto);
//    }
//
//
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
//        employeeService.deleteEmployee(id);
//        return  ResponseEntity.status(HttpStatus.NO_CONTENT).build();
//    }

}
