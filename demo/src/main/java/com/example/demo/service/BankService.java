package com.example.demo.service;

import com.example.demo.model.Bank;

import java.util.List;

public interface BankService {
    List<Bank> getAllEmployee();
    Bank getEmployeeById(Long id);
//    void createEmployee(EmployeeDto employeeDto);
//    void updateEmployee(Long id, EmployeeDto employeeDto);
//    void deleteEmployee(Long id);

}
